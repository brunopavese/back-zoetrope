const { response } = require('express');
const Post = require('../models/Post');

// Create
const create = async(req,res) => {
	try{
			const user = await Post.create(req.body);
			return res.status(201).json({user: user});
	}catch(err){
			res.status(500).json({error: err});
		}
};
// Read
const index = async(req,res) => {
	try {
		const users = await Post.findAll();
		return res.status(200).json({users});
	}catch(err){
		return res.status(500).json({err});
	}
};

const show = async(req,res) => {
	const {id} = req.params;
	try {
		const user = await Post.findByPk(id);
		return res.status(200).json({user});
	}catch(err){
		return res.status(500).json({err});
	}
};
//Update
const update = async(req,res) => {
	const {id} = req.params;
	try {
		const [updated] = await User.update(req.body, {where: {id: id}});
		if(updated) {
			const user = await User.findByPk(id);
			return res.status(200).send(user);
		}
		throw new Error();
	}catch(err){
		return res.status(500).json("Publicação não encontrada");
	}
};
//Delete
const destroy = async(req,res) => {
	const {id} = req.params;
	try {
		const deleted = await User.destroy({where: {id: id}});
		if(deleted) {
			return res.status(200).json("Publicação deletada com sucesso");
		}
		throw new Error ();
	}catch(err){
		return res.status(500).json("Publicação não encontrada");
	}
};

module.exports = {
	index,
	show,
	create,
	update,
	destroy
};