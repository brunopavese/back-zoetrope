const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

// Definição da model com os atributos
const Post = sequelize.define('Post', {
    post_id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        unique: true
    },

    text: {
		type: DataTypes.TEXT,
        allowNull: false
	},

	date: {
		type: DataTypes.DATEONLY,
		allowNull: false
	}


},{
	//timestamps = false
});

// Definições das relações
//Post.associate = function(models) {
//	Post.belongsTo(models.User, {foreignKey: "userId"});
//}

module.exports = Post;