const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

// Definição da model com os atributos
const User = sequelize.define('User', {
    user_id: {
        type: DataTypes.INTEGER,
        allowNull:  false,
        primaryKey: true,
        unique: true
        
    },
	email: {
		type: DataTypes.STRING,
		allowNull: false
	},

	name: {
		type: DataTypes.STRING,
		allowNull: false
	},

    last_name: {
		type: DataTypes.STRING,
        allowNull: false
	},

	date_of_birth: {
		type: DataTypes.DATEONLY,
		allowNull: false
	},

	phone_number: {
		type: DataTypes.STRING,
	}


},{
	//timestamps = false
});

// Definições das relações
User.associate = function(models) {
	User.hasMany(models.Post, {foreignKey: "userId"});
    User.hasMany(models.User, {through: "Follow", as: "followedUser"})
    User.belongsToMany(models.User, {through: "Follow", as: "followerUser"})
}

module.exports = User;