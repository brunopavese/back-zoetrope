const { Router } = require('express');
const UserController = require('../controllers/UserController');
const PostController = require('../controllers/PostController');
const router = Router();

router.get('/users', UserController.index);
router.get('/users/:id', UserController.show);
router.post('/users', UserController.create);
router.put('/users/:id', UserController.update);
router.delete('/users/:id', UserController.destroy);

router.get('/post', PostController.index);
router.get('/post/:id', PostController.show);
router.post('/post', PostController.create);
router.put('/post/:id', PostController.update);
router.delete('/post/:id', PostController.destroy);

module.exports = router;
